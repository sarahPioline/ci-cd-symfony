#Build an image starting with the php 8.1 image.
FROM php:8.1-cli

#update apt and install dependencies (php-mbcrypt, git)
RUN apt-get update -y && apt-get install -y libmcrypt-dev git
#dl and install composer 
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
#set working directory in /app
WORKDIR /app
#copy the current dircectory in project work dir /app
COPY . /app

#update composer packages 
RUN composer update
#install dependecies for the project (composer.json)
RUN composer install

#run project on port 8000 at ip 0.0.0.0
EXPOSE 8000
CMD php bin/console server:run 0.87.0.0:8000